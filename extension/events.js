var url = "http://192.168.120.130:5000/verify";

function verify(request, sender, sendResponse) {
  console.log(request.payload);
  $.ajax({
    url: url,
    data: request.payload,
    method: "POST",
    dataType: "html"
  }).then(function(resp) {
    sendResponse({
      success: true,
      result: resp
    });
  }, function(err) {
    sendResponse({
      success: false,
      error: err
    });
  });
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.type == "verify") {
    verify(request, sender, sendResponse);
  }

  return true;
});
