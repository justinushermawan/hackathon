var methods = {
  verify() {
    chrome.runtime.sendMessage({
      type: "verify",
      payload: {
        title: this.article.title,
        url: this.article.url,
        date: Math.floor(new Date().getTime() / 1000),
        image_link: this.article.imageUrl,
      }
    }, (data) => {
      if (!data.success) return;
      var value = data.result;
      if (value < 0) value = 0.0;
      this.score.verifying = false;
      this.score.value = Number(value) / 20;
    });
  }
};

export default function() {
  return new Vue({
    el: this.root[0],
    data: () => ({
      article: null,
      score: {
        verifying: true,
        value: 0.0
      }
    }),
    methods
  });
}
