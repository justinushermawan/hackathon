import selectors from "content/selectors";
import createVue from "./createVue";
import URL from "url";

function findElements(root, selectors) {
  var result = {};
  _.each(selectors, (selector, name) => {
    if (_.isString(selector)) {
      result[name] = root.find(selector).eq(0);
    } else {
      result[name] = findElements(root, selector);
    }
  });
  return result;
}

function parseExternalLink(url) {
  return URL.parse(url, true).query.u;
}

export default class Article {
  constructor(el) {
    this.root = el instanceof jQuery ? el : $(el);
    this.isArticle = this.root.find(selectors.article.caption).length > 0;
    this.data = {};
    this.props = {
      href: Article.getID(this.root)
    };
    this.id = this.props.href;
  }

  mount() {
    this.el = findElements(this.root, selectors.story);
    this.el.article = findElements(this.root, selectors.article);
    this.el.meta.append("<badge :score='score'></badge>");
    this.vue = createVue.call(this);
    return this;
  }

  parse() {
    var content = this.el.content;
    var article = this.el.article;
    this.data.story = content.text();
    this.data.article = {
      title: article.title.text(),
      caption: article.caption.text(),
      meta: article.meta.text(),
      imageUrl: article.image.attr("src"),
      url: parseExternalLink(article.link.attr("href"))
    };
    this.vue.article = this.data.article;
    return this;
  }

  verify() {
    this.vue.verify();
  }

  init() {
    this.mount();
    this.parse();
    this.verify();
    return this;
  }

  static getID(el) {
    return $(`${selectors.story.meta} a`, el).attr("href");
  }
}
