import storySelector from "content/selectors/story.json";
import Article from "content/models/Article";

var read = {};
function readStories() {
  $(storySelector.article).each(function() {
    var article = new Article(this);
    if (!article.isArticle) return;
    if (article.id && !read[article.id]) {
      article.init();
      read[article.id] = article;
    }
  });
}

$(function() {
  readStories();
  console.log(read);

  var oldHeight = document.body.scrollHeight;
  setInterval(() => {
    var newHeight = document.body.scrollHeight;
    if (newHeight > oldHeight) {
      readStories();
    }
  });
});
