import VueResource from "vue-resource";
import Badge from "content/components/badge.vue";

Vue.use(VueResource);
Vue.component("badge", Badge);
