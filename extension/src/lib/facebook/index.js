import Facebook from "facebook-node-sdk";
import settings from "./settings.json";

const facebook = new Facebook(settings);
export default facebook;
