export default function(type, payload) {
  return new Promise((resolve, reject) => {
    chrome.runtime.sendMessage({
      type, payload
    }, (response) => {
      console.log(response);
      if (response.success) {
        resolve(response);
      } else {
        reject(response);
      }
    });
  });
}
