var gulp = require("gulp");
var gutil = require("gulp-util");
var notify = require("gulp-notify");
var webpack = require("webpack");
var webpackConfig = require("./webpack.config");

gulp.task("default", ["build"]);

gulp.task("build", ["webpack"]);

gulp.task("watch", ["webpack:watch"]);

function webpackCallback(cb) {
    var done = false;
    return function(err, stats) {
        if (err) {
            notify("Webpack build error!");
            throw new gutil.PluginError("[webpack]", err);
        }

        gutil.log("[webpack]", stats.toString());
        if (!done) {
            done = true;
            cb();
        }
    };
}

gulp.task("webpack", function(cb) {
    webpack(webpackConfig, webpackCallback(cb));
});

gulp.task("webpack:watch", function(cb) {
    var config = Object.assign({ watch: true }, webpackConfig);
    webpack(config, webpackCallback(cb));
});
