var webpack = require("webpack");
var path = require("path");

module.exports = {
  devtool: "eval",

  entry: {
    content: "./src/content/index",
    // vendor: ["jquery", "lodash", "vue", "vue-resource"]
  },

  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].js"
  },

  module: {
    loaders: [{
      test: /\.js$/,
      loader: "babel",
      include: path.join(__dirname, "src"),
      exclude: path.join(__dirname, "node_modules")
    }, {
      test: /\.json$/,
      loader: "json"
    }, {
      test: /\.scss$/,
      loader: "style!css?modules!sass"
    }, {
      test: /\.css$/,
      loader: "style!css?modules"
    }, {
      test: /\.html$/,
      loader: "raw"
    }, {
      test: /\.vue$/,
      loader: "vue"
    }]
  },

  resolve: {
    root: [
      path.join(__dirname, "node_modules"),
      path.join(__dirname, "src")
    ],
    alias: {
      "vue$": "vue/dist/vue.common.js"
    }
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      _: "lodash",
      jQuery: "jquery",
      Vue: "vue"
    }),
    /*
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      minChunks: Infinity
    })
    */
  ]
};
