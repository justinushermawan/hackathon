var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');

var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var dataCached;

var apiUrl = 'https://hack.kurio.co.id/v1/';
var token = 'N7mtmDeRHwMo8EkIwHnxeQEeEJc5IY9NfSrYgsUy';

var headers = {
      'Accept': 'application/json',
      'X-Kurio-Client-ID': '99',
      'X-Kurio-Client-Secret': 'S3VyaW9IYWNrYXRvbjIw',
      'X-OS': 'windows',
      'X-App-Version': '1',
      'Authorization': 'Bearer ' + token
    };

var axises = [
      {
        "type": "topic",
        "id": 99,
        "name": "News"
      },
      {
        "type": "topic",
        "id": 40,
        "name": "Technology"
      },
      {
        "type": "topic",
        "id": 32,
        "name": "Business & Economy"
      },
      {
        "type": "topic",
        "id": 58,
        "name": "Entertainment"
      },
      {
        "type": "topic",
        "id": 59,
        "name": "Fun & Unique"
      },
      {
        "type": "topic",
        "id": 65,
        "name": "Travel"
      },
      {
        "type": "topic",
        "id": 51,
        "name": "Soccer"
      },
      {
        "type": "topic",
        "id": 76,
        "name": "Healthy Living"
      },
      {
        "type": "topic",
        "id": 63,
        "name": "Lifestyle"
      }
    ];

router.get('/news2', function(req, res, next){

  request({
      url: apiUrl + 'feed/topic:40?num=20',
      method: 'GET',
      headers: headers
    }, function(error, response, body){
      var data = JSON.parse(body).data;
      var allTitles = [];
      data.forEach(dd => {
        allTitles.push(dd.title);
      });

      res.send(JSON.parse(body));
    });
});

router.get('/explore', (req, res, next) => {
  request({
      url: apiUrl + 'explore',
      method: 'GET',
      headers: headers
    }, function(error, response, body){
      res.send(JSON.parse(body));
    });
});

router.get('/exploreGroup', (req, res, next) => {
  request({
      url: apiUrl + 'explore/group/15',
      method: 'GET',
      headers: headers
    }, function(error, response, body){
      res.send(JSON.parse(body));
    });
});

router.get('/article', (req, res, next) => {
  request({
      url: apiUrl + 'article/2607',
      method: 'GET',
      headers: headers
    }, function(error, response, body){
      res.send(JSON.parse(body));
    });
});

router.get('/news', function(req, res, next){

  var allTitles = [];

  var count = 0;

  var returnPromise = new Promise((resolve, reject) => {
      var returnPromiseResolver = resolve;

      axises.forEach(axis => {

      var axisString = axis.type + ':' + axis.id + '?num=20';

      var axisPromise = new Promise((resolve, reject) => {
        request({
          url: apiUrl + 'feed/' + axisString,
          method: 'GET',
          headers: headers
        }, (error, response, body) => {
          var data = JSON.parse(body);
          data.data.forEach(dd => {
            allTitles.push(dd.title);
          });

          resolve();
        });
      });

      axisPromise.then((r) => {
          count++;
          if(count >= axises.length)
            returnPromiseResolver();
        });
    });
  });

  returnPromise.then((r) => res.send(allTitles));
});

router.post('/news', function(req, res, next){

  var allTitles = [];

  var titleFilter = req.body.title;
  var timestamp = req.body.timestamp;

  var count = 0;

  var returnPromise = new Promise((resolve, reject) => {

    // if(dataCached != undefined)
    // {
    //   allTitles = dataCached.slice(0);
    //   resolve();
    // }

      var returnPromiseResolver = resolve;

      axises.forEach(axis => {

      var axisString = axis.type + ':' + axis.id + '?num=40';

      var axisPromise = new Promise((resolve, reject) => {
        request({
          url: apiUrl + 'feed/' + axisString,
          method: 'GET',
          headers: headers
        }, (error, response, body) => {
          if(error)
          {
            resolve();
          }
          else
          {
            var data = JSON.parse(body);
            data.data.forEach(dd => {
              // for timestamp
              // var rTimestampInt = parseInt(timestamp);
              // var dTimestampInt = parseInt(dd.timestamp);
              
              // if(rTimestampInt <= dTimestampInt + 100 && rTimestampInt >= dTimestampInt - 100)
                allTitles.push(dd.title);
            });

            resolve();
          }
        });
      });

      axisPromise.then((r) => {
          count++;
          if(count >= axises.length)
            returnPromiseResolver();
        });
    });
  });

  returnPromise.then((r) => {

      if(dataCached == undefined)
        dataCached = allTitles.slice(0);

      var values = [];

      for(var i = 0; i<allTitles.length; i++)
      {
        values.push({
          heuristicValue: calcSimilarity(titleFilter, allTitles[i]),
          stringValue: allTitles[i]
        });
      }

      values = values.sort(stringSortComparator);

      var sortedString = [];
      var maxHeuristic = {value: 0};
      values.forEach(v => {
        if(v.heuristicValue > maxHeuristic.value)
          maxHeuristic.value = v.heuristicValue;
        sortedString.push(v.stringValue);
      });
      
      res.send(sortedString);
    });
});

function stringSortComparator(a, b)
{
  if(a.heuristicValue < b.heuristicValue)
    return 1;

  if(a.heuristicValue > b.heuristicValue)
    return -1;

  return 0;
}

function calcSimilarity(a, b)
{
  var longer = a.length > b.length ? a : b;
  var shorter = (longer === a) ? b : a;

  var maxLength = longer.length;
  if(maxLength == 0)
    return 1.0;

  return (maxLength - calcEditDistance(longer, shorter)) / parseFloat(maxLength);
}

function calcEditDistance(s1, s2) {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  var costs = [];
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i == 0)
        costs[j] = j;
      else if (j > 0) {
          var newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
              costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
      }
    }
    if (i > 0)
      costs[s2.length] = lastValue;
  }
  return costs[s2.length];
}

module.exports = router;
