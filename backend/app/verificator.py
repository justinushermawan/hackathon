import httplib
import json
import urllib
import urllib2
import math
import difflib
import unicodedata

IBM_WATSON_API_KEY = "a30f07459a5be1e92b3820dba7b9320dfb5f3715"
WOT_API_KEY = "75a24006a6a2e9de96645e1de6dd761cf1abcc5a"
TW_CONSUMER_KEY = "oCRK8ldF5VkMWjpSUUlpBA"
TW_CONSUMER_SECRET = "ChsD0d2Z69yX7uz64ajRsVpp2sgLH1P2UgcqsVSFc"
OAUTH_TOKEN = "166484314-yy2sUkxwj3ODojYYHfTop3xbnQQmCPKRf1BJyBSq"
OAUTH_SECRET = "tt8xjffEz7wmGl1pSnU3mYwEzNLjOd91EIAxD402gOw"
MICROSOFT_CV_SUBSCRIPTION_KEY = "beda2c5b6a9c4778834b580b2ab65944"
MICROSOFT_BING_SEARCH_SUBSCRIPTION_KEY = "6201f66d2c5c4d66b288b30cde75feb9"

WOT_API_ENDPOINT = "http://api.mywot.com/0.4/public_link_json2"
KURIO_API_ENDPOINT = "http://192.168.120.129:4000/news"

def verify_link(url):
	import requests
	query_string = {"hosts" : "/" + url + "/", "callback" : "process", "key" : WOT_API_KEY}
	payload = ""
	headers = {
		'content-type': "application/x-www-form-urlencoded",
		'cache-control': "no-cache",
		'postman-token': "93ffde57-c70f-a775-d5ce-03f8e152e9da"
	}
	response = requests.request("GET", WOT_API_ENDPOINT, data=payload, headers=headers, params=query_string)
	data = response.text.replace("process", "")
	wot_score = int(data.split("[")[1].split(",")[0])
	return wot_score

def get_link_title(link):
	from watson_developer_cloud import AlchemyLanguageV1
	alchemy_language = AlchemyLanguageV1(api_key=IBM_WATSON_API_KEY)
	alchemyres = json.dumps(alchemy_language.title(url=link), indent=2)
	data = json.loads(alchemyres)
	return data["title"]

def ever_tweeted(link):
	import re
	from twython import Twython
	u = 0
	tworno = 0
	thetwtext = ""
	twusers = []

	tw = Twython(TW_CONSUMER_KEY,
				TW_CONSUMER_SECRET,
				OAUTH_TOKEN,
				OAUTH_SECRET)

	headers = {
		'Content-Type': 'application/json',
		'Ocp-Apim-Subscription-Key': MICROSOFT_CV_SUBSCRIPTION_KEY,
	}
	params = urllib.urlencode({'language': 'unk', 'detectOrientation': 'true'})
	body = "{\"url\":\"" + link + "\"}"
	try:
		conn = httplib.HTTPSConnection('api.projectoxford.ai')
		conn.request("POST", "/vision/v1.0/ocr?%s" % params, body, headers)
		response = conn.getresponse()
		json_data = response.read()
		data = json.loads(json_data.decode("utf-8"))

		if len(data) == 3:
			if data["orientation"] == 'NotDetected':
				u = 1
		else:
			for d in data['regions'][0]['lines']:
				for y in d['words']:
					thetwtext = thetwtext + " " + y['text']
					if '@' in y['text']:
						twusers.append(y['text'])
						tworno = 1
					elif y['text'].lower() in ["tweet", "retweets"]:
						tworno = 1
					else:
						pass
		conn.close()
	except Exception as e:
		print(e)
	twpresentornot = 0
	if tworno == 0:
		u = 1
	elif tworno == 1:
		for o in twusers:
			o = o.replace("@", "")
			try:
				u_timeline = tw.get_user_timeline(screen_name=o, count=1000)
			except TwythonError as e:
				pass
			for tweets in u_timeline:
				formattedtweets = str(tweets['text'].encode('utf-8')).replace("b\'", "").replace("\'", "")
				formattedtweets = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', formattedtweets)
				if formattedtweets in thetwtext:
					twpresentornot = 1
					break

	returned = 1 if twpresentornot == 1 else 0
	return u == 1 or returned != 0

def google_search(search):
	search=search.encode("utf-8")
	headers = {'User-agent': 'Chrome/41.0.2228.0'}
	req = urllib2.Request('http://www.google.com/search?q=' + urllib2.quote(search), None, headers)
	site = urllib2.urlopen(req)
	data = site.read()
	site.close()

	start = data.find('<div id="res">')
	end = data.find('<div id="foot">')

	if data[start:end] == '':
		return False
	else:
		links = []
		titles = []
		data = data[start:end]
		start = 0
		end = 0
		while start > -1 and end > -1:
			index = data.find('<a href="/url?q=')
			index = data.find('">', index + 16)
			data = data[index + 2:]
			end = data.find('</a>', 0)
			this_title = data[0:end]
			this_title=this_title.replace("<b>","");
			this_title=this_title.replace("</b>","");
			if this_title.find("<img") == -1:
				titles.append(this_title)
			data = data[end + 4:]

			start = data.find('<cite>')
			end = data.find('</cite>')
			this_link = data[start + 6:end]
			links.append(this_link)
			data = data[end + 7:]

			index = data.find('<a href="')
			data = data[index:]

	return titles

def compare_title(title_main, title_master):
	title_main = title_main.lower().encode("utf-8")
	title_master = title_master.lower().encode("utf-8")
	return difflib.SequenceMatcher(None, title_main, title_master).ratio()

def get_kurio_data(title, url):
	import requests
	data = urllib.urlencode({'title': title, 'timestamp' : '102131221'})
	req = urllib2.Request(url, data)
	response = urllib2.urlopen(req)
	news_array = json.loads(response.read())
	max_persentase = 0
	hasil_persentase = 0
	for news in news_array:
		try:
			hasil_persentase = compare_title(title.encode("utf-8"), news.encode("utf-8"))
		except Exception as e:
			pass
		if hasil_persentase > max_persentase:
			max_persentase = hasil_persentase
	return max_persentase * 100

def get_google_data(judul):
	titles = google_search(judul)
	max_persentase = 0
	hasil_persentase = 0
	for title in titles:
		try:
			hasil_persentase = compare_title(judul.encode("utf-8"), title.encode("utf-8"))
		except Exception as e:
			pass
		if hasil_persentase > max_persentase:
			max_persentase = hasil_persentase
	return max_persentase * 100

def verify(url, title, img_link=None, date=None):
	try:
		presentase_wot = verify_link(url);
		judul = get_link_title(url).encode("utf-8")
		presentase_google = get_google_data(judul)
		presentase_kurio = get_kurio_data(judul, KURIO_API_ENDPOINT)
		presentase_twitter = 100
		if len(img_link) > 0:
			if(ever_tweeted(img_link) == False):
				presentase_twitter = 0
		total_persentase = presentase_wot * 0.2 + presentase_kurio * 0.15 + presentase_google * 0.6 + presentase_twitter * 0.05
		return str(total_persentase)
	except Exception as e:
		print(e)
		return str(-1)

def main(link):
	link = 'http://bola.liputan6.com/read/2442623/dampingi-rio-haryanto-orangtua-terbang-ke-spanyol'
	presentase_wot = verify_link(link);
	judul = get_link_title(link).encode("utf-8")
	presentase_google = get_google_data(judul)
	presentase_kurio = get_kurio_data(judul, KURIO_API_ENDPOINT)
	presentase_twitter = 100
	if(ever_tweeted(link) == False):
		presentase_twitter = 0
	total_persentase = presentase_wot * 0.2 + presentase_kurio * 0.15 + presentase_google * 0.6 + presentase_twitter * 0.05
	return total_persentase

if __name__ == '__main__':
	print(main('https://news.detik.com/berita/3355310/jadi-tersangka-isu-rush-money-ini-postingan-abu-uwais-di-facebook?utm_source=facebook&utm_medium=oa&utm_content=news&utm_campaign=cms+socmed'))