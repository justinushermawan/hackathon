import os
from flask import Flask, request, render_template
from flask.ext.cors import CORS, cross_origin
from flask_sslify import SSLify
from verificator import main, verify

app = Flask(__name__)
sslify = SSLify(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/')
@cross_origin()
def hello():
	return str(main('ahok'))

@app.route('/verify', methods=['POST'])
def verifying():
	title = request.form['title']
	image_link = request.form['image_link']
	url = request.form['url']
	date = request.form['date']
	return verify(url, title, image_link, date)

if __name__ == '__main__':
	port = int(os.environ.get("PORT", 5000))
	app.run(debug=True, host='0.0.0.0', port=port)